{
    //optional, default and only supported value is "cpc"
    "type": "cpc"
    //optional, default is EUR
    "currency": "EUR",
    
    //at least one budget is mandatory
    "budget": 10,
    "daily_budget": 5,
    
    //mandatory
    "price": 0.5,
    
    //(asap|even)
    //asap - budget is spent as fast as possible
    //evenly - will try to split budget evenly by day
    "budget_strategy": "evenly",
    
    //(asap|even)
    //asap - budget is spent as fast as possible
    //even - will try to split budget evenly by hour
    "daily_budget_strategy": "asap"
}