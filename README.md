# Available endpoints:
```
(POST) /public_api/v1/{partner_id}/login

(GET|POST) /public_api/v1/{partner_id}/advertisers
(GET|PUT) /public_api/v1/{partner_id}/advertisers/{id}

(GET|POST) /public_api/v1/{partner_id}/campaigns
(GET|PUT) /public_api/v1/{partner_id}/campaigns/{id}
(GET|POST) /public_api/v1/{partner_id}/campaigns/{campaignId}/ads

(GET|PUT) /public_api/v1/{partner_id}/ads/{id}
```

# Auth
Get token by:
```
POST /public_api/v1/{partner_id}/login
```
See [login_object.js](login_object.js)

# General guidelines

## Creating objects
To create a new obejct you must make a POST request to the appropriate endpoint, the post should contain a valid object in json format, with all the mandatory fields. It **MUST NOT** contain the "id" field.
In case the request was successful the API will respond with:
- 200 OK status code
- the body of the response will contain the full json of the newly created object including the "id" field
 
## Updating objects
To update an existing object you must make a PUT request to the appropriate endpoint, the request body should contain the object in json format
 
## Getting/Deleting a resource
To get or delete a resource it's enough to make a simple GET or DELETE http request to the appropriate endpoints
 
## API Response in case something goes wrong: 
In case of missing fields or invalid values the API will respond with:
- 400	Bad Request
- response body will be a json containing more information about what went wrong

In case the auth token is missing/invalid/expired:
- 401	Unauthorized

In case the requested resource was not found:
- 404	Not Found
    
# Campaigns
## Listing campaigns:
```
GET /public_api/v1/{partner_id}/campaigns
```
## Creating campaigns:
Get, insert, update campagin:
```
(GET|POST|PUT) /public_api/v1/{partner_id}/campaigns/{id}
```
In case of success the api will respond with a 200 OK and a campaign object
See [campaign_object.js](campaign_object.js)

## Creating creatives:
Get or insert ad:
```
(GET|POST) /public_api/v1/{partner_id}/campaigns/{campaignId}/ads
```
In case of success the api will respond with a 200 OK and a creative object
The creative will be added to the campaign "creatives" field

See [campaign_object.js](creative_object.js)

## Managing creatives:
Get or update ad:
```
(GET|PUT) /public_api/v1/{partner_id}/ads/{id}
```

# Advertisers
List or insert advertiser
```
(GET|POST) /public_api/v1/{partner_id}/advertisers
```

Get or update advertiser
```
(GET|PUT) /public_api/v1/{partner_id}/advertisers/{id}
```
See [advertiser_object.js](advertiser_object.js)
  
# TODO:
- add DELETE method
- stats
