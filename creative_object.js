{
    //generated by api
    //omit when creating a new object
    "id" : "590c547bad818c00078c2awe",
    //optional, default false
    "disabled" : true,
    //mandatory
    "name" : "Test creative",
    //mandatory if format is "native", omit otherwise
    //max 25 characters
    "title" : "Test creative title",
    //mandatory if format is "native", omit otherwise
    //max 90 characters
    "description" : "Test creative description",
    //mandatory if format is "native", omit otherwise
    //max 15 characters
    "cta" : "Find out more",
    //mandatory
    //can be one of the following: "native", "leaderboard", "mediumrect"
    //native -> native rendering mode; "title", "description" and "cta" fields are mandatory in this mode
    //leaderboard -> 728x90 IAB Leaderboard format
    //mediumrect -> 300x250 IAB Medium rectangle format
    //For leaderboard and mediumrect formats, "title", "description" and "cta" should be omitted
    "format" : "native",
    //mandatory
    "image": "https://sd.keepcalm-o-matic.co.uk/i-w600/keep-calm-and-caca-maca.jpg",
    //mandatory
    "click_url" : "http://www.ividence.com",
    //mandatory
    "brand" : {
        //mandatory
        //max 25 characters
        "name" : "Super Cristal",
        //optional
        "url": "http://supercristal.ro",
        //optional
        "logo": "http://www.1001cosmetice.ro/images/largeimages/super-cristal-anticarie-pasta-de-dinti-1467902857-4.jpg"
    }
}