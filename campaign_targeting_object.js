{
    //optional
    "publisher_domain": {
        "value": ["example.com"],
        //True if you want to target everything except the above values
        "exclude": false
    },
    //targeting based on newsletter iab categories
    //iab subcategories are not supported and will be ignored
    "categories":{
        "value": ["IAB1","IAB2-1"],
         //True if you want to target everything except the above values
        "exclude": false
    },
    //optional
    "location":{
        //optional
        //targeting based on user country
        "countries":{
            "value": ["FRA","BEL"],
            //True if you want to target everything except the above values
            "exclude": false
        } 
    }
}