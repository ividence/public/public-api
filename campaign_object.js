{
    //generated by api
    //omit when creating a new campaign object
    "id" : "590c52130f1e650007b809e7",
    //mandatory, must exist
    "advertiser_id" : "596634e0dce76b43e0fb73ca",
    //mandatory
    "name" : "Test creative",
    //optional, default - false
    "disabled" : false,
    //mandatory
    "start_date" : "2017-05-05T09:47:25Z",
    //mandatory
    "end_date" : "2017-05-31T09:47:25Z",
    //mandatory
    //will determine in which newsletters the campaign is delivered
    "language" : "fr",
    //mandatory, these fields refer to the campaign and creative iab categories
    "categories" : ["IAB2","IAB2-1","IAB3"],
    //mandatory, see pricing_model_object.json
    "pricing_model" : {...},
    //optional, see campaign_targeting_object.json
    "targeting" : {...},
    //read-only array of creative objects
    //use dedicated api to manage creatives for campaign
    //see creative_object.json
    "creatives" : [{...}, {...}]
}
